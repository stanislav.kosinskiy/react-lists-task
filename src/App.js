import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ValidationComponent from './ValidationComponent/ValidationComponent';
import CharComponent from './CharComponent/CharComponent';

class App extends Component {

  state = {
    text: ''
  }

  textChangedHandler = (event) => {
    this.setState({
      text: event.target.value
    });
  };

  charClickHandler = (charIndex) => {
    this.setState({
      text: this.state.text.split('').filter( (char, index) => charIndex !== index ).join('')
    });
  }

 
  render() {

    let charComponents = this.state.text.split('');
    charComponents =
      <div>
        {charComponents.map( (char, index) => <CharComponent key={index} letter={char} click={()=>this.charClickHandler(index)} />)}
      </div>;
    return (
      <div>
        <input type="text" onChange={this.textChangedHandler} value={this.state.text}/>
        <p>{this.state.text.length}</p>
        <ValidationComponent textLength={this.state.text.length} />
        {charComponents}
      </div>
    );
  }
}

export default App;
